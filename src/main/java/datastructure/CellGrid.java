package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState grid[][];
    private CellState initialState;


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows=rows;
        this.columns=columns;
        this.grid = new CellState[rows][columns];
        
        for(int i=0;i<rows;i++){
            for(int k=0;k<columns;k++){
                grid[i][k]=initialState;
            }
        }
  
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row<0 || row>numRows()){
            throw new IndexOutOfBoundsException("Row should be greater than or equal to 0, and less than the total number of rows.");
        }

        else if(column<0 || column>numColumns()){
            throw new IndexOutOfBoundsException("Column should be greater than or equal to 0, and less than the total number of columns.");
        }

        else{
            grid[row][column]=element;
        }

        }
        

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copygrid = new CellGrid(numRows(), numColumns(), initialState);
        for(int i=0;i<rows;i++){
            for(int k=0;k<columns;k++){
                copygrid.set(i, k, get(i,k));
            }
        }
        return copygrid;
    }
    
}
