package cellular;

import datastructure.IGrid;

import java.util.Random;

import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int r=0;r<numberOfRows();r++){
			for (int c=0; c<numberOfColumns();c++){
				nextGeneration.set(r, c, getNextCell(r, c));
			}
		}
		currentGeneration=nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState currentState = currentGeneration.get(row,col);
		if (currentState==CellState.ALIVE){
			return CellState.DYING;
		}
		if (currentState==CellState.DYING){
            return CellState.DEAD;
        }
        else{
            if (countNeighbors(row, col, CellState.ALIVE)==2){
                return CellState.ALIVE;
            }
            
            else return CellState.DEAD;
            
        }
    }
	

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int returnstate = 0;
		for (int r = -1; r<=1;r++) {
			for (int c = -1; c<=1;c++){
				if (r==0 && c==0){
					continue;
				}
				if (row+r<0 || col+c<0 || row+r>=numberOfRows() || col+c>=numberOfColumns()) {
					continue;
				}
				if (getCellState(row+r, col+c)==state){
					returnstate++;
				}
				else continue;
			}


		}
		return returnstate;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}

    
    

